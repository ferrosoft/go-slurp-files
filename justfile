set dotenv-load := true

openssh_url := "https://cdn.openbsd.org/pub/OpenBSD/OpenSSH/portable/openssh-${OPENSSH_VERSION}.tar.gz"
openssh_prefix := justfile_directory() + "/openssh/root"
openssh_build_dir := justfile_directory() + "/openssh/build"
openssh_extract_dir := justfile_directory() + "/openssh"
openssh_tarball := "openssh.tar.gz"

sshd := openssh_prefix + "/sbin/sshd"
ssh_keygen := openssh_prefix + "/bin/ssh-keygen"

testserver_files := justfile_directory() + "/testserver"
sshd_config := testserver_files + "/sshd_config"
sshd_pidfile := testserver_files + "/sshd.pid"
sshd_logfile := testserver_files + "/sshd.log"
authorized_keys := testserver_files + "/authorized_keys"
id_rsa := testserver_files + "/id_rsa"

test: start-sshd
    go test ./...

clean: stop-sshd
    rm -r openssh
    rm {{sshd_config}} \
        {{sshd_logfile}} \
        {{authorized_keys}} \
        {{id_rsa}} \
        {{id_rsa}}.pub \
        {{openssh_tarball}}

fetch-openssh:
    #!/usr/bin/env bash
    set -euxo pipefail
    if [ ! -f "{{openssh_tarball}}" ]; then
        curl -o {{justfile_directory()}}/openssh.tar.gz -SL {{ openssh_url }}
    fi
    echo "${OPENSSH_SHA256} {{openssh_tarball}}"

extract-openssh: fetch-openssh
    mkdir -p {{openssh_extract_dir}}
    tar -C {{openssh_extract_dir}} -xf {{openssh_tarball}}

build-openssh: extract-openssh
    #!/usr/bin/env bash
    set -euxo pipefail
    if [ -x "{{openssh_build_dir}}/sshd" ]; then
      echo "sshd is up to date"
      exit 0
    fi
    mkdir -p {{openssh_build_dir}}
    cd {{openssh_build_dir}}
    ../openssh-${OPENSSH_VERSION}/configure --prefix {{openssh_prefix}}
    make

install-openssh: build-openssh
    #!/usr/bin/env bash
    set -euxo pipefail
    if [ -x "{{sshd}}" ]; then
        echo "sshd is already installed"
        exit 0
    fi
    mkdir -p {{openssh_prefix}}
    cd {{openssh_build_dir}}; make install

generate-client-keys:
    if [ ! -f "{{id_rsa}}" ]; then \
        {{ssh_keygen}} -f {{id_rsa}} -N test; \
    fi

start-sshd: generate-client-keys stop-sshd
    # For test server only a single key is required. Creating authorized_keys is
    # a simple matter of copying the public key.
    cp {{id_rsa}}.pub {{authorized_keys}}
    # Some settings in sshd_config require absolute paths. Copy the stock file
    # and modify settings in the copy.
    cp {{sshd_config}}.dist {{sshd_config}}
    sed -r -i "s#^(AuthorizedKeysFile).*#\1 {{authorized_keys}}#" {{sshd_config}}
    sed -r -i "s#^(PidFile).*#\1 {{sshd_pidfile}}#" {{sshd_config}}
    # Run sshd pointing to modified config.
    {{sshd}} -E {{sshd_logfile}} -f {{sshd_config}}

stop-sshd:
    if [ -f "{{sshd_pidfile}}" ]; then \
        pkill -F "{{sshd_pidfile}}"; \
    fi
