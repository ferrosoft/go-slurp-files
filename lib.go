package slurpfiles

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"path"

	"github.com/moov-io/go-sftp"
	aggregaterr "gitlab.com/ferrosoft/go-aggregate-error"
)

// File is the remote file being processed.
type File interface {
	// Name is the absolute file name.
	Name() string
	// LocalFile is the file in the local filesystem.
	//
	// Remote contents are downloaded into this file.
	LocalFile() *os.File
}

type MoovSFTPFile struct {
	f        *go_sftp.File
	tempFile *os.File
}

func NewMoovSFTPFile(f *go_sftp.File) (result MoovSFTPFile, err error) {
	// Write contents into local temporary file. This makes it rewindable.
	temp, err := os.CreateTemp("", "remote")
	if err != nil {
		err = fmt.Errorf("opening temp file: %v", err)
		return
	}

	_, err = io.Copy(temp, f.Contents)
	if err != nil {
		err = fmt.Errorf("copying into temp file: %v", err)
		return
	}

	_, err = temp.Seek(0, io.SeekStart)
	if err != nil {
		err = fmt.Errorf("rewinding temp file: %v", err)
		return
	}

	result.f = f
	result.tempFile = temp

	return
}

func (moov *MoovSFTPFile) LocalFile() *os.File {
	return moov.tempFile
}

func (moov *MoovSFTPFile) Name() string {
	return moov.f.Filename
}

// BackendFunc fetches files from its backend and sends them on the sink.
//
// Implementations must close the channel when they are done and return.
type BackendFunc func(ctx context.Context, sink chan<- File) error

// OnFileFunc processes file.
type OnFileFunc func(ctx context.Context, file File) error

// Thunk is a callback for various, though purely side-effect reasons.
type Thunk func(ctx context.Context) error

// Handler includes callbacks for use in Digest.
type Handler interface {
	// OnFile is invoked on any file encountered.
	OnFile(ctx context.Context, file File) error

	// OnCompletion is invoked once after all files have been handled.
	//
	// It is invoked regardless of success and before OnSuccess.
	OnCompletion(ctx context.Context) error

	// OnSuccess is invoked once after all files have been handled.
	//
	// It is invoked only if no error occured.
	OnSuccess(ctx context.Context) error
}

// Digest runs the backend and invokes callbacks.
func Digest(ctx context.Context, backend BackendFunc, handler Handler) error {
	results := make(chan File)
	errs := aggregaterr.New("digesting: ")
	var backendErr error

	go func() {
		err := backend(ctx, results)
		if err != nil {
			backendErr = err
		}
	}()

Loop:
	for {
		select {
		case file, more := <-results:
			if !more {
				break Loop
			}
			err := handler.OnFile(ctx, file)
			if err != nil {
				errs.Append(err)
				continue Loop
			}
		case <-ctx.Done():
			errs.Append(ctx.Err())
			break Loop
		}
	}

	// backendErr must be appended here to avoid mutex in aggregate error.
	if backendErr != nil {
		errs.Append(backendErr)
	}

	err := handler.OnCompletion(ctx)
	if err != nil {
		errs.Append(err)
	}

	err = errs.Return()
	if err != nil {
		return err
	}

	return handler.OnSuccess(ctx)
}

// SFTPBackend fetches files via SFTP.
func SFTPBackend(client go_sftp.Client, directory string) BackendFunc {
	return func(ctx context.Context, sink chan<- File) error {
		defer close(sink)

		fileNames, err := client.ListFiles(directory)
		if err != nil {
			return fmt.Errorf("failed reading SFTP directory %s: %w", directory, err)
		}

		errs := aggregaterr.New("failed reading some SFTP files: ")

		for _, fileName := range fileNames {
			remoteFile, err := client.Reader(fileName)
			if err != nil {
				errs.Append(err)
				continue
			}
			adapter, err := NewMoovSFTPFile(remoteFile)
			if err != nil {
				errs.Append(err)
				continue
			}
			sink <- &adapter
		}

		return errs.Return()
	}
}

// SequentialHandlers enables chaining of handlers.
type SequentialHandlers struct {
	// Handlers to be executed sequentially.
	//
	// For OnFile handlers, if one returns an error, subsequent handlers
	// do not run. Local file is rewinded to the start after each handler,
	// to make the file rereadable.
	//
	// For other handlers, if one returns an error, subsequent
	// handlers still run, and any errors are propagated upward as an
	// aggregated error.
	Handlers []Handler
}

func NewSequentialHandlers(handlers ...Handler) *SequentialHandlers {
	return &SequentialHandlers{
		Handlers: handlers,
	}
}

func (seq *SequentialHandlers) OnFile(ctx context.Context, file File) error {
	for _, h := range seq.Handlers {
		err := h.OnFile(ctx, file)
		if err != nil {
			return err
		}

		_, err = file.LocalFile().Seek(0, io.SeekStart)
		if err != nil {
			return fmt.Errorf("rewinding file: %v", err)
		}
	}
	return nil
}

func (seq *SequentialHandlers) OnCompletion(ctx context.Context) error {
	errs := aggregaterr.New("OnCompletion: ")
	for _, h := range seq.Handlers {
		err := h.OnCompletion(ctx)
		if err != nil {
			errs.Append(err)
		}
	}
	return errs.Return()
}

func (seq *SequentialHandlers) OnSuccess(ctx context.Context) error {
	errs := aggregaterr.New("OnSuccess: ")
	for _, h := range seq.Handlers {
		err := h.OnSuccess(ctx)
		if err != nil {
			errs.Append(err)
		}
	}
	return errs.Return()
}

// MoveFilesToArchive is a specialized handler, moving files to archive folder.
//
// This is a common pattern seen when exchanging files via SFTP.
type MoveFilesToArchive struct {
	// ArchiveDirectory is the directory name on remote server
	// where files are moved to.
	ArchiveDirectory string

	// Client is used for remote operations.
	Client go_sftp.Client
}

func (mov *MoveFilesToArchive) OnFile(ctx context.Context, file File) error {
	inboxFile := file.Name()
	baseName := path.Base(inboxFile)
	archiveFile := fmt.Sprintf("%s/%s", mov.ArchiveDirectory, baseName)

	log.Printf("archiving file %s to %s\n", inboxFile, archiveFile)

	err := mov.Client.UploadFile(
		archiveFile,
		io.NopCloser(file.LocalFile()),
	)
	if err != nil {
		return fmt.Errorf("uploading file: %v", err)
	}

	err = mov.Client.Delete(inboxFile)
	if err != nil {
		return fmt.Errorf("deleting file: %v", err)
	}

	return nil
}

func (mov *MoveFilesToArchive) OnCompletion(ctx context.Context) error {
	return nil
}

func (mov *MoveFilesToArchive) OnSuccess(ctx context.Context) error {
	return nil
}
