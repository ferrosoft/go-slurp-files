# slurp-files

Process files from SFTP backend. See the test for a complete example. To run the
test use [just](https://github.com/casey/just).

## Running tests

```
docker build -t slurp-test .
docker run --rm -it slurp-test
```

## Origin

This is a fork of https://codeberg.org/sterndata/go-slurp-files
