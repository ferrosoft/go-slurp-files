FROM golang:1.22-alpine
RUN apk update && apk add bash \
    curl \
    gcc \
    just \
    make \
    musl-dev \
    openssl-dev \
    shadow \
    zlib-dev
RUN groupadd -g 1000 slurp && useradd -u 1000 -g 1000 -m -d /usr/src/app slurp
WORKDIR /usr/src/app
COPY justfile .env .
RUN just install-openssh
COPY . .
RUN chown -R slurp:slurp .
USER slurp:slurp
ENTRYPOINT just
