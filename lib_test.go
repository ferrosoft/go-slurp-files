package slurpfiles

// This component test assumes a SFTP server is running on localhost:9999.
// There are a few more hardcoded assumptions down there. Dockerfile takes
// care of running the test.

import (
	"context"
	"fmt"
	"io"
	"os"
	"os/user"
	"path"
	"testing"
	"time"

	"github.com/moov-io/base/log"
	go_sftp "github.com/moov-io/go-sftp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type testHandler struct{
	Should *assert.Assertions
	ExpectedContent string
	OnFileInvocations int
	OnCompletionInvocations int
	OnSuccessInvocations int
}

func (h *testHandler) OnFile(ctx context.Context, file File) error {
	localFile := file.LocalFile()
	h.Should.Equal("hello.txt", path.Base(file.Name()))
	contents, err := io.ReadAll(localFile)
	h.Should.Nil(err)
	h.Should.Equal(h.ExpectedContent, string(contents))
	h.OnFileInvocations++
	return nil
}

func (h *testHandler) OnCompletion(ctx context.Context) error {
	h.OnCompletionInvocations++
	return nil
}

func (h *testHandler) OnSuccess(ctx context.Context) error {
	h.OnSuccessInvocations++
	return nil
}


type erroringHandler struct{
	ErrorOnFile bool
	ErrorOnCompletion bool
	ErrorOnSuccess bool
}

func (e *erroringHandler) OnFile(ctx context.Context, file File) error {
	if e.ErrorOnFile {
		return fmt.Errorf("failed OnFile")
	}
	return nil
}

func (e *erroringHandler) OnCompletion(ctx context.Context) error {
	if e.ErrorOnCompletion {
		return fmt.Errorf("failed OnCompletion")
	}
	return nil
}

func (e *erroringHandler) OnSuccess(ctx context.Context) error {
	if e.ErrorOnSuccess {
		return fmt.Errorf("failed OnSuccess")
	}
	return nil
}


func sftpClientForTesting() (client go_sftp.Client, err error) {
	currentUser, err := user.Current()
	if err != nil {
		return
	}

	pemBytes, err := os.ReadFile("testserver/id_rsa")
	if err != nil {
		return
	}

	client, err = go_sftp.NewClient(log.NewNopLogger(), &go_sftp.ClientConfig{
		Hostname: "localhost:9999",
		Username: currentUser.Username,
		Timeout: 5*time.Second,
		MaxConnections: 1,
		ClientPrivateKey: string(pemBytes),
		ClientPrivateKeyPassword: "test",
	})
	return
}


func TestComponent(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()

	curDir, err := os.Getwd()
	must.Nil(err)
	filesDir := curDir + "/testserver/files"

	sftpClient, err := sftpClientForTesting()
	must.Nil(err, "SFTP client failed")

	handler := &testHandler{
		Should: assert.New(t),
		ExpectedContent: "hello, world\n",
	}
	err = Digest(
		ctx,
		SFTPBackend(sftpClient, filesDir),
		handler,
	)
	must.Nil(err)
	must.Equal(1, handler.OnFileInvocations, "OnFile was not invoked")
	must.Equal(1, handler.OnSuccessInvocations, "OnSuccess was not invoked")
	must.Equal(1, handler.OnCompletionInvocations, "OnCompletion was not invoked")
}


func TestSequentialHandler(t *testing.T) {
	must := require.New(t)
	should := assert.New(t)
	ctx := context.Background()

	curDir, err := os.Getwd()
	must.Nil(err)
	filesDir := curDir + "/testserver/files"

	sftpClient, err := sftpClientForTesting()
	must.Nil(err, "SFTP client failed")

	firstHandler := &testHandler{
		Should: should,
		ExpectedContent: "hello, world\n",
	}
	secondHandler := &testHandler{
		Should: should,
		ExpectedContent: "hello, world\n",
	}

	err = Digest(
		ctx,
		SFTPBackend(sftpClient, filesDir),
		NewSequentialHandlers(firstHandler, secondHandler),
	)
	must.Nil(err)
	must.Equal(1, firstHandler.OnFileInvocations, "OnFile was not invoked in firstHandler")
	must.Equal(1, firstHandler.OnSuccessInvocations, "OnSuccess was not invoked in firstHandler")
	must.Equal(1, firstHandler.OnCompletionInvocations, "OnCompletion was not invoked in firstHandler")
	must.Equal(1, secondHandler.OnFileInvocations, "OnFile was not invoked in secondHandler")
	must.Equal(1, secondHandler.OnSuccessInvocations, "OnSuccess was not invoked in secondHandler")
	must.Equal(1, secondHandler.OnCompletionInvocations, "OnCompletion was not invoked in secondHandler")
}


func TestMoveFilesToArchive(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()

	curDir, err := os.Getwd()
	must.Nil(err)
	inboxDir := curDir + "/testserver/files/move/inbox"
	archiveDir := curDir + "/testserver/files/move/archive"

	sftpClient, err := sftpClientForTesting()
	must.Nil(err, "SFTP client failed")

	handler := &testHandler{
		Should: assert.New(t),
		ExpectedContent: "How are you?\n",
	}
	err = Digest(
		ctx,
		SFTPBackend(sftpClient, inboxDir),
		NewSequentialHandlers(handler, &MoveFilesToArchive{
			ArchiveDirectory: archiveDir,
			Client: sftpClient,
		}),
	)
	must.Nil(err)
	must.Equal(1, handler.OnFileInvocations, "OnFile was not invoked")
	must.Equal(1, handler.OnSuccessInvocations, "OnSuccess was not invoked")
	must.Equal(1, handler.OnCompletionInvocations, "OnCompletion was not invoked")

	// As a side effect, hello.txt must have moved from inbox to archive.
	inboxFile, err := sftpClient.Reader(inboxDir + "/hello.txt")
	must.NotNil(err, "Reading inbox file should return error")
	must.Nil(inboxFile, "Inbox file should not exist")

	archiveFile, err := sftpClient.Reader(archiveDir + "/hello.txt")
	must.Nil(err, "Reading archive file should work")
	must.NotNil(archiveFile, "Archive file should exist")

	contents, err := io.ReadAll(archiveFile.Contents)
	must.Nil(err, "reading archive file contents")
	must.Equal("How are you?\n", string(contents))
}


func TestError(t *testing.T) {
	must := require.New(t)
	ctx := context.Background()

	curDir, err := os.Getwd()
	must.Nil(err)
	filesDir := curDir + "/testserver/files"

	sftpClient, err := sftpClientForTesting()
	must.Nil(err, "SFTP client failed")

	backend := SFTPBackend(sftpClient, filesDir)

	err = Digest(ctx, backend, &erroringHandler{ErrorOnFile: true})
	must.ErrorContains(err, "failed OnFile")

	err = Digest(ctx, backend, &erroringHandler{ErrorOnCompletion: true})
	must.ErrorContains(err, "failed OnCompletion")

	err = Digest(ctx, backend, &erroringHandler{ErrorOnSuccess: true})
	must.ErrorContains(err, "failed OnSuccess")

	handler := &testHandler{
		Should: assert.New(t),
		ExpectedContent: "hello, world\n",
	}

	err = Digest(ctx, backend, NewSequentialHandlers(
		&erroringHandler{ErrorOnFile: true},
		handler,
	))
	must.ErrorContains(err, "failed OnFile")

	err = Digest(ctx, backend, NewSequentialHandlers(
		handler,
		&erroringHandler{ErrorOnFile: true},
	))
	must.ErrorContains(err, "failed OnFile")
}
